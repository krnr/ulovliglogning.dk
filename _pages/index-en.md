---
layout: indexpage
lang: en
permalink: /
sections:
  - id: intro
    heading: Let's stop the mass surveillance!
    content: |
      The mass surveillance conducted by European governments has on two occasions been struck down by the European Court of Justice. Most recently this happened in 2016 when the court reviewed the joint case of Tele2 from Sweden and Tom Watson et al from England and Wales. However Danish politicians and telco industry refuse to honor fundamental rights and continue to retain data illegally.

      We are a motley crew of human rights advocates and privacy campaigners that have come together to use the courts to send a simple message: Honour the law and respect our fundamental rights.

      In cooperation with our attorneys, we are currently working on the subpœna to file suit against the minister of justice, Søren Pape Poulsen. You can download the current draft (in Danish) [here (pdf)](https://ulovliglogning.dk/assets/files/staevning.pdf)
  - id: butwhy
    link: Why?
    heading: But why?
    content: |
      You might not think that your secrets are worth keeping, but with enough data, everyone’s a suspect. Maybe you’ve sent a Merry Christmas text to the suspect of an ongoing investigation? Maybe you’ve googled “manure” the week before a homemade bomb is found?

      Maybe you’re the only person with a mobile phone that’s been near the scene of a crime, and maybe you’d just bought binliners. Mass surveillance flips the burden of proof. Your movements are recorded and can be used against you, but not in your defense. You might have left your phone at home on purpose.

  - id: tellmemore
    link: How?
    heading: Tell me more!
    content: |
      The Danish telecom industry is still retaining data, in violation of the human rights of their customers. Attempting to justify their illegal actions, the Telecom Association have published a letter from the minister of justice, Søren Pape Poulsen, telling them to disregard the law. 
      
      The minister's letter had no legal foundation, and we have decided to file suit against the state. A democratic society cannot tolerate officials threatening citizens and companies with prosecution if they honor the law.
  - id: wannahelp
    link: Help us!
    heading: I want to help!
    content: |
      You can contact spokesperson [Rasmus Malver](https://twitter.com/rasmusmalver) on [Twitter](https://twitter.com/rasmusmalver) or via [SMS/Signal](sms:+4526809424).

      You can also transfer money to <span class="donate">IBAN: DK3953010000272500, SWIFT: ALBADKKK</span>. We also accept bitcoins on <span class="donate">3KCgp9THXoETK2qxE2TiPKpCCt5EYuombG</span> and Mobile Pay to 40456. We have already reached 450.000 DKK, but we still need your help! The next goal is 1.000.000 DKK.
      
      The designated attorney for the Danish government, Kammeradvokaten, has access to unlimited means. When your main customer prints their own money, you are free to charge a substantial sum.

      You can also help us gain momentum through publicity. Contact your network, journalists, friends, and family - and explain why it is important to fight for our rights.
  - id: faq
    link: FAQ
    content:
    - heading: Why Søren Pape?
      answer: He is Minister of Justice.
      explanation: | 
        Søren Pape is the relevant minister. Data retention was equally illegal when Søren Pind (V), Mette Frederiksen (S), Karen Hækkerup (S), Morten Bødskov (S) and Brian Arthur Mikkelsen (K) held the office. It is not a political issue. It is about honoring the law and not violating the fundamental rights.

        This case will, hopefully, change Danish politicians obvious and intentional violations of our human rights.
    - heading: Why not TDC?
      answer: Pape wrote a letter telling them to surveil illegally.
      explanation: | 
        TDC claim ignorance of the law. With the letter from the minister, they argue that they lack the ability to understand the illegality of a ministerial order.

        It will be more difficult and more expensive to argue a case against the state of Denmark. But it will set the precedent for future governments wanting to oppress the fundamental rights of the people.
    - heading: Why should I care? If surveillance can hinder crime, I'm pro data retention!
      answer: It is a myth that data retention helps the police.
      explanation: | 
        Mass surveillance does not necessarily prevent or stop crime. It **can** lead to more arrests and fewer unsolved crimes, but mainly because innocent people will be punished. Having a database table of the location of unsolved crimes and a database table of everybody's movements makes it possible to collate data and prove that everybody is guilty.

        Have you bought a crowbar recently or have you walked past a house that has been burgled? Mass surveillance requires you to [doublethink](https://en.wikipedia.org/wiki/Doublethink) to stay out of jail.

        Honoring human rights does not leave the police blind as bats. Crime could be solved before 2006, but the police have to think for themselves. Who might be a suspect and why? It will still be possible to conduct surveillance, but only with sufficient democratic oversight.
    - heading: Who's behind this?
      answer: Foreningen imod Ulovlig Logning (The Association Against Illegal Surveillance).
      explanation: | 
        Human rights jurist [Rasmus Malver](https://twitter.com/rasmusmalver) kickstarted the fundraising with a contribution of 30.000 DKK, and the second large donor is [Bitbureauet](https://bitbureauet.dk/). In January the movement  gained traction and today 100s of people, businesses and organization have donated. An organization was created to hold the money, and it will all be used to pay the legal fees.
    
        We have chosen IT- and EU-specialists [Bird & Bird](https://www.twobirds.com), and lawyer Martin von Haller leads the team. 
    - heading: What is the relationship to human rights?
      answer: You have a right to privacy.
      explanation: | 
        Human rights are your rights against governmental abuse. Some countries have human rights enshrined in their constitutions, but in Denmark, they mainly exist in the form of the European Convention on Human Rights ([pdf](http://www.echr.coe.int/Documents/Convention_ENG.pdf)). It was written after the second world war to avoid history repeating, and over time it has been updated to protect minorities and to avoid the emerging terrors from states on both sides of the iron curtain.

        The people of the European Union has agreed upon an updated version of the convention in 2000, the EU Charter on Fundamental Rights ([pdf](http://www.europarl.europa.eu/charter/pdf/text_en.pdf)), where the protection of privacy is emphasized.

        The Danish mass surveillance and retention of metadata is an obvious violation of both conventions, and it has been established that both are important parts of Danish law. The current surveillance is more intrusive than the surveillance conducted by both Gestapo and STASI.

        You have the right not be the victim of this.
    - heading: Are you profiting from this? What will happen if there's too much money?
      answer: No. And excess funding will be forwarded to a similar case or organization.
      explanation: | 
        All of the funds will go to paying legal fees and the attorney, ([Bird & Bird](https://www.twobirds.com)). If we end up with excess money (unlikely), it will be forwarded to a similar case or organization.
    - heading: What is being retained about me?
      answer: Among other things what phone you have, where you are and who you're communicating with.
      explanation: | 
        The location of your mobile phone at all times of every day. Who you communicate with and, to some degree, what you do online. You can ask your provider for a copy of the data. If they reply within a reasonable time, they are allowed to charge you up to 200 DKK for it.
    - heading: The minister says he needs time to replace the legislation. Isn't that ok?
      answer: No.
      explanation: | 
        When mass surveillance was brought in on EU level, politicians were told it violates human rights, and thus it would be illegal. When implemented in Denmark they were told the same. When the ministerial notice was issued they were told again.

        A ministerial notice (bekendtgørelse) cannot exist without a legal basis in a law, and a law implementing EU regulation cannot exist when the regulation has been struck down.
    
        The European Court of Justice was unusually clear when striking down the surveillance regulation in both the case of Digital Rights and Tele2/Watson. It did not come as a surprise to anybody, and obviously, it withdrew any legal basis from the ministerial notice.

        Justice minister Søren Pape Poulsen wants to continue the surveillance and is desperately looking for a way to sneak it through parliament. There is a large majority for doing it, but it would require Danish secession from the Council of Europe and potentially the European Union. Denmark would then stand with Belarus as one of only two European states not a party to the Convention on Human Rights.

        Metadata retention is a criminal act, and the minister is only kept out of jail by a constitutional provision stating that only other politicians can bring him to justice.
---
