document.documentElement.classList.remove("no-js");
document.documentElement.classList.add("js");

function closeMobileMenu(){
	var menu = document.getElementById("show-menu");
	menu.checked = false;
}

var mobileLinks = document.getElementsByClassName("mobileLink");
for (var i = 0; i < mobileLinks.length; i++) {
    mobileLinks[i].addEventListener('click', closeMobileMenu);
}